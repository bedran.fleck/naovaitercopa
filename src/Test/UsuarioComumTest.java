package Test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Model.Endereco;
import Model.UsuarioComum;

public class UsuarioComumTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		
		Endereco endereco = new Endereco("Brasil", "DF", "BSB", "Gama", "UnB", "OO", 7);
		UsuarioComum usuario = new UsuarioComum();
		
		usuario.setNome("nome");
		assertEquals("nome", usuario.getNome());
		usuario.setEmail("hey");
		assertEquals("hey", usuario.getEmail());
		usuario.setSenha("senha");
		assertEquals("senha", usuario.getSenha());
		usuario.setEndereco(endereco);
		assertEquals(endereco, usuario.getEndereco());
	}

}
