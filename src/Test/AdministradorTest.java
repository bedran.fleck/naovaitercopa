package Test;

import Model.Administrador;
import Model.Endereco;
import Model.UsuarioComum;
import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import Model.Administrador;

public class AdministradorTest {

	@Before
	public void setUp() throws Exception {
		
	}

	@Test
	public void test() {
		Administrador usuario = new Administrador();
		assertEquals(true, usuario.isADM());
		Endereco endereco = new Endereco("Brasil", "DF", "BSB", "Gama", "UnB", "OO", 7);
		
		usuario.setNome("nome");
		assertEquals("nome", usuario.getNome());
		usuario.setEmail("hey");
		assertEquals("hey", usuario.getEmail());
		usuario.setSenha("senha");
		assertEquals("senha", usuario.getSenha());
		usuario.setEndereco(endereco);
		assertEquals(endereco, usuario.getEndereco());
	}

}
