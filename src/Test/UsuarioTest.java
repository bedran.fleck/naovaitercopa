package Test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Model.Endereco;
import Model.Usuario;

public class UsuarioTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		Usuario usuario = new Usuario("Andre", "a@mail.com", "senha");
		assertEquals("Andre", usuario.getNome());
		assertEquals("a.mail.com", usuario.getEmail());
		assertEquals("senha", usuario.getSenha());
	}

}
