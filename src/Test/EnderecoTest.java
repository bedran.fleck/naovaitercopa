package Test;
import Model.Endereco;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class EnderecoTest {

	@Before
	public void setUp() throws Exception {
		
	}

	@SuppressWarnings("deprecation")
	@Test
	public void test() {
		Endereco endereco = new Endereco();
		endereco.setPais("Brasil");
		endereco.setEstado("DF");
		endereco.setCidade("BSB");
		endereco.setBairro("Gama");
		endereco.setLogradouro("UnB");
		endereco.setComplemento("OO");
		endereco.setNumero(7);
		assertEquals("Brasil", endereco.getPais());
		assertEquals("DF", endereco.getEstado());
		assertEquals("BSB", endereco.getCidade());
		assertEquals("Gama", endereco.getBairro());
		assertEquals("UnB", endereco.getLogradouro());
		assertEquals("OO", endereco.getComplemento());
		assertEquals(7, endereco.getNumero());
	
	}

}
