package Test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import Model.Reivindicacao;

public class ReivindicacaoTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		Reivindicacao proposta = new Reivindicacao();
		proposta.setDescricao("descricao");
		proposta.setTitulo("titulo");
		assertEquals("descricao", proposta.getDescricao());
		assertEquals("titulo", proposta.getTitulo());
	}

}
