/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author alunos
 */
public class Administrador extends Usuario{
    public boolean isADM = true;

    public Administrador(String nome, String email, String senha, Endereco endereco) {
        super(nome, email, senha, endereco);
    }
    
    public Administrador() {
		// TODO Auto-generated constructor stub
	}
    
    public boolean isADM() {
		return isADM;
	}
}
