/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author alunos
 */
public class Endereco {
    private String pais;
    private String estado;
    private String cidade;
    private String bairro;
    private String logradouro;
    private String complemento;
    private double numero;

    public Endereco(String pais, String estado, String cidade, String bairro, String logradouro, String complemento, double numero) {
        this.pais = pais;
        this.estado = estado;
        this.cidade = cidade;
        this.bairro = bairro;
        this.logradouro = logradouro;
        this.complemento = complemento;
        this.numero = numero;
    }

    public Endereco() {
    }
    

    public String getBairro() {
        return bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public String getComplemento() {
        return complemento;
    }

    public String getEstado() {
        return estado;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public double getNumero() {
        return numero;
    }

    public String getPais() {
        return pais;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public void setNumero(double numero) {
        this.numero = numero;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }
    
    
}
