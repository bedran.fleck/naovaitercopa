/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author alunos
 */
public class Reivindicacao {
    private String descricao;
    private String titulo;
    private Usuario autor;

    public Reivindicacao(String descricao, String titulo, Usuario autor) {
        this.descricao = descricao;
        this.titulo = titulo;
        this.autor = autor;
    }
    
    public Reivindicacao(String descricao, String titulo) {
        this.descricao = descricao;
        this.titulo = titulo;
    }
    
    public Reivindicacao() {
		// TODO Auto-generated constructor stub
	}

    public Usuario getAutor() {
        return autor;
    }

    public String getDescricao() {
        return descricao;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setAutor(Usuario autor) {
        this.autor = autor;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    
}
